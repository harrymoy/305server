var passport = require('passport-restify'),
TwitterStrategy = require('passport-twitter').Strategy,
session = require('restify-session');

passport.use(new TwitterStrategy({
    consumerKey: config.twitter.consumerKey,
    consumerSecret: config.twitter.consumerSecret,
    callbackURL: "http://127.0.0.1:8080/auth/twitter/callback"
  },
  function(token, tokenSecret, profile, done) {
    User.findOrCreate({ twitterId: profile.id }, function (err, user) {
      return done(err, user);
    });
  }
));

server.get('/auth/twitter', passport.authenticate('twitter'), function(req, res){});
server.get('/auth/twitter/callback', passport.authenticate('twitter', {failureRedirect: '/'}),
	function(req, res) {
		res.redirect('/account');
	});