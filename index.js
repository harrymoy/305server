/*jshint esnext: true */
var restify = require('restify'),
	mongoose = require('mongoose'),
	stocks = require('./stocks.js'),
	basicAuth = require('./basicAuth.js'),
	db = require('./mongo.js');

var server = restify.createServer();

server.use(restify.fullResponse());
server.use(restify.bodyParser());
server.use(restify.authorizationParser());

server.get('/', function(req, res, next){
	res.redirect('/stocks', next);
	const auth = req.authorization.basic.username;
	const pass = req.authorization.basic.password;
	console.log("Username is: " + auth);
	console.log("Password is: " + pass);
});

server.get('/mystocks', function(req, res) {
	stocks.getAllStocksFromDB(data => {
		res.setHeader('content-type', 'application/json');
		res.send(data);
		res.end();
	});
});

server.get('/stocks/remove/:symbol', function(req, res) {
	//Using 'use strict' for function scoped variables using via 'let'
	'use strict';
	//Getting symbol parameter and putting it into upper case for Mongo query
	let symbol = req.params.symbol.toUpperCase();
	stocks.removeStockBySymbol(symbol, data => {
		res.setHeader('content-type', 'application/json');
		//Send data from the stock.js function
		res.send(data);
		res.end();
	});
});

server.get('/login/:token', function(req, res){
	'use strict';
	let token = req.params.token;
	db.checkToken(token, data => {
		if (token === data) {
			//Checking that token is acceptable
			res.setHeader('content-type', 'application/json');
			res.send(data.code, 'user logged in');
			res.end();
		} else {
			res.setHeader('content-type', 'application/json');
			res.send(data.code, 'invalid token');
			res.end();
		}
	});
});

server.get('/mystocks/:symbol', function(req, res) {
	'use strict';
	//Getting stock from DB with that symbol
	let symbol = req.params.symbol;
	stocks.getStockFromDB(symbol, data => {
		res.setHeader('content-type', 'application/json');
		res.send(data);
		res.end();
	});
});

server.get('/stocks', function(req, res){
	'use strict';
	let message = "Specify a stock in the URL";
	res.setHeader('content-type', 'application/json');
	res.send(message.code, message);
	res.end();
   	// mongo.addStock(data);
});

server.get('/stocks/:symbol', function(req, res) {
	'use strict';
	//Getting symbol from URL
	let symbol = req.params.symbol;
	//Getting authorization data from request header
	let user = req.authorization.basic.username;
	let pass = req.authorization.basic.password;
	//Checking if user and pass is acceptable
	if (user === "harry" && pass === "pass") {
		//Do function to get data for that symbol
		stocks.getCompanyData(symbol, data => {
			res.setHeader('content-type', 'application/json');
			//Sending the callback from the function
			res.send(data.code, data);
			res.end();
		});
	} else {
		//If credentials are not good
		res.setHeader('content-type', 'application/json');
		res.send("Insufficient Details");
		res.end();
	}
});

server.get('/stocks/:userToken', function(req, res) {
	'use strict';
	let userTokenForRequest = req.params.userToken;
	
});

server.post('/stocks/:symbol', function(req, res){
	'use strict';
	let symbol = req.params.symbol;
	let comment = req.body;
	stocks.addCommentToStock(symbol, comment, data => {
		console.log("data being returned " + data);
		res.setHeader('content-type', 'application/json');
		console.log("Code is:" + data.code);
		res.send(data.code, data);
		db.addStock(data => {
			console.log("Request sent");
		});
		res.end();
	});
});

server.put('/stocks', function(req, res){
	res.setHeader('content-type', 'application/json');
});

//Hosting the server
var port = process.env.PORT || 8080;
server.listen(port, function(err) {
	if (err) {
		console.log(err);
	} else {
		console.log("Module is ready at port " + port);
	}
});