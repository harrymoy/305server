/*jshint esnext: true */
const btoa = require('btoa');

exports.createToken = (username, password, callback) => {
	var encryptedUser = createUserPartToken(username);
	var encryptedPass = createPassPartToken(password);
	var token = encryptedUser + encryptedPass;
	callback(token);
};


var createUserPartToken = function(username) {
	var encryptedUser = btoa(username);
	return encryptedUser;
};

var createPassPartToken = function(password) {
	var encryptedPass = btoa(password);
	return encryptedPass;
};