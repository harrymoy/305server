/*jshint esnext: true */
var sync = require("sync-request");
var auth = require("./basicAuth.js");
var db = require("./mongo.js");

const ENDPOINT = "https://finance.yahoo.com/webservice/v1/symbols/"; //Having the string as a global variable makes it easier to change should the string need to be changed
const RETURN_FORMAT = "/quote?format=json"; //Same reason for implementation as above.

//Arrow function to call the two functions and create a JSON formatted callback
exports.getCompanyData = (symbol, callback) => {
	console.log(symbol);
	//Gets data from the API call functions
	const shareValue = getStockValue(symbol);
	const nameOfCompany = getCompanyName(symbol);
	//Puts symbol in upper case
	symbol = symbol.toUpperCase();
	console.log("share value = " + shareValue);
	console.log("company name = " + nameOfCompany);
	const result = {symbol: symbol, name: nameOfCompany, price: shareValue};
	//Adding the stock to MongoDB
	db.addStock(result, data => {
		console.log("Stock being added");
	});
	console.log("the result is " + result);
	//Passing result into callback
	callback(result);
};

exports.addCommentToStock = (symbol, comment, callback) => {
	const stockComment = comment;
	symbol = symbol.toUpperCase();
	const stockName = getCompanyName(symbol);
	//Creating a result object that will be passed into the callback
	const result = {name: stockName, symbol: symbol, comment: stockComment};
	callback(result);
};

exports.getStockFromDB = (symbol, callback) => {
	//Passing symbol argument into getStockBySymbol
	db.getStockBySymbol(symbol, data => {
		callback(data);
	});
};

exports.getAllStocksFromDB = callback => {
	//Getting all stocks
	db.getAllStocks(data => {
		callback(data);
	});
};

exports.removeStockBySymbol = (symbol, callback) => {
	//Passing symbol argument into RemoveBySymbol
	db.removeBySymbol(symbol, data => {
		callback(data);
	});
};

//Using async basis from taxi.js lab
//Making call to Yahoo to get the company's share price
var getStockValue = function(symbol) {
	'use strict';
	const url = ENDPOINT + symbol + RETURN_FORMAT;
	//Get request added
	let res = sync('GET', url);
	//Parsing result JSON
	let data = JSON.parse(res.getBody().toString('utf8'));
	//Parsing the data as a float
	let price;
	//If the symbol doesn't work then 
	if (data.list.meta.count === 0 || data.list.resources[0].resource.fields.volume === "0"){
		var lseSymbol = symbol + ".L";
		try {
			//Do function again with new symbol
			price = getStockValue(lseSymbol);
		} catch(err) {
			console.log("Error with LSE symbol" + err);
		}
	} else {
		price = parseFloat(data.list.resources[0].resource.fields.price);
	}
	//Returning the data that will be used by function calling this function
	return price;
};

//Making call to Yahoo to get the company's name
var getCompanyName = function(symbol) {
	const url = ENDPOINT + symbol + RETURN_FORMAT;
	var res = sync('GET', url);
	var data = JSON.parse(res.getBody().toString('utf8'));
	var companyName;
	if (data.list.meta.count === 0 || data.list.resources[0].resource.fields.volume === "0") {
		var lseSymbol = symbol + ".L";
		try {
			companyName = getCompanyName(lseSymbol);
			}
		catch(err) {
			console.log("Error with LSE symbol" + err);
		}
	} else{
		companyName = data.list.resources[0].resource.fields.name;
	}
	if (companyName === null) {
		return "No Data";
	}
	//Returning the data that will be used by function calling this function
	return companyName;
};