/*jshint esnext: true */
var mongoose = require('mongoose');
var database = 'stocks';

const server = 'mongodb://localhost/'+database;
console.log(server);

//Connecting to the database
mongoose.connect(server);
const db = mongoose.connection;

const stockSchema = new mongoose.Schema({
	//Creating the schema for Stock
	symbol: {type: String, required: true},
	company: {type: String, required: true},
	price: {type: Number, required: true }
});

const Stock = mongoose.model('Stock', stockSchema);

exports.checkToken = (token, callback) => {
	User.find({token: token}, (error, data) => {
		if (error) {
			callback("Error is " + error );
		}
		callback(data);
	});
};

exports.addStock = (data, callback) => {
	console.log("Data is" + data);
	//Parsing data object passed into function
	const name = data.name;
	const symbol = data.symbol;
	const value = data.price;
	//Creating Stock schema object that is added to database
	const newStock = new Stock({symbol: symbol, company: name, price: value});
	//Saving to collection
	newStock.save((err, data) => {
		if (err) {
			callback("Error" + err);
		}
		callback("Data added" + data);
	});
	console.log("Stock added" + newStock);
};

exports.getAllStocks = callback => {
	//Getting all data from the collection
	Stock.find ((error, data) => {
		if (error) {
			callback("There was an error" + error);
		}
		const stocksList = data.map(share => {
			return {symbol: share.symbol, company: share.company, price: share.price};
		});
		callback(stocksList);
	});
};

exports.getStockBySymbol = (symbol, callback) => {
	//Getting all data that has the same symbol as the symbol parameter
	Stock.find({symbol: symbol}, (error, data) => {
		if (error) {
			callback("error is" + error);
		}
		callback(data);
	});
};

exports.removeBySymbol = (symbol, callback) => {
	//Removing data that has the same symbol as the symbol parameter
	Stock.remove({symbol: symbol}, error => {
		if (error) {
			callback("error" + error);
		}
		callback("Stock removed");
	});
};
	
exports.removeAll = callback => {
	Stock.remove({}, error => {
		if (error) {
			callback("error" + error);
		}
		callback("All stocks cleared");
	});
};