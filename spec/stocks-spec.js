/* jshint esnext: true */
const stocks = require("../stocks.js");

//unexpected token in test is due to double parsing of JSON

describe('Stocks', function () {

	it('should get name for Alphabet Inc', function (done) {
		stocks.getCompanyData('GOOG', function(data) {
			console.log(data.name);
			expect(data.name).toBe('Alphabet Inc.');
			done();
		});
	});

	it('should get name for Apple', function(done) {
		stocks.getCompanyData('AAPL', function(data) {
			console.log(data.name);
			expect(data.name).toBe('Apple Inc.');
			done();
		});
	});

	it('should get value for Apple', function(done) {
		stocks.getCompanyData('AAPL', function(data) {
			console.log(data.price);
			expect(data.price).toBeGreaterThan(100);
			done();
		});
	});

	it('should get value for Sainsburys using LSE handling', function(done) {
		stocks.getCompanyData('SBRY', function(data){
			console.log(data.price);
			console.log(data.name);
			expect(data.name).toBe('SAINSBURY');
			expect(data.price).toBeGreaterThan(200);
			done();
		});
	});

	it('should remove all stocks for Apple', function(done) {
		stocks.removeStockBySymbol('AAPL', function(data){
			expect(data).toBe("Stock Removed");
			stock.getStockFromDB('AAPL', function(data){
				expect(data).toBe(null);
				console.log("Data: " + data);
			});
		});
		done();
	});
});